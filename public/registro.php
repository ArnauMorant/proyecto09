<?php
require "../src/conexion.php";
require "../src/usuario.php";
  $r=new Usuario();
  $error=$r->comprobarCampos($_POST);
  $resultado=$r->user();
  if(isset($error)){
    if($error===false){
      $r->registrarse();
    }
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Proyecto</title>
    <link rel="stylesheet" href="css/master.css">
  </head>
  <body>
<?php require "assets/header.php" ?>
      <div class="form">
      <form method="post">
      <br>
      <label class="nmb" for="nombre">Nombre:</label>
      <br>
      <input type="text" name="nombre" id="nombre">
      <div id="hidden1" style="display:none;">No puede dejarse en blanco</div>
      <br><br>
      <label class="apl" for="apellidos">Apellidos:</label>
      <br>
      <input type="text" name="apellidos" id="apellidos">
      <div id="hidden2" style="display:none;">No puede dejarse en blanco</div>
      <br><br>
      <label class="tlf" for="telefono">Telefono:</label>
      <br>
      <input type="text" name="telefono" id="telefono">
      <div id="hidden3" style="display:none;">No puede dejarse en blanco</div>
      <div id="hidden4" style="display:none;">Tiene que utilizar numeros</div>
      <br><br>
      <label class="crs" for="curso">Curso:</label>
      <br>
      <input type="text" name="curso" id="curso">
      <div id="hidden5" style="display:none;">No puede dejarse en blanco</div>
      <div id="hidden6" style="display:none;">Tiene que utilizar numeros</div>
      <br><br>
      <div class="div1reg">
      <input class="enviar" type="submit" value="Enviar" onclick="return comprobar()">
      </div>
      <br>
    </form>
    </div>
    <script type="text/javascript" src="js/comprobarRegistro.js"></script>
    <?php require "assets/footer.php" ?>
  </body>
</html>
