<?php
require "../src/conexion.php";
require "../src/usuario.php";
  $u=new Usuario();
  $resultado=$u->user();
  $resultado2=$u->tabla();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Proyecto</title>
    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </head>
  <body>
<?php require "assets/header.php" ?>
    <table align=center border=1px>
          <td style="text-align:center;padding:25px">ID</td>
          <td style="text-align:center;padding:25px">Nombre</td>
          <td style="text-align:center;padding:25px">Apellidos</td>
          <td style="text-align:center;padding:25px">Telefono</td>
          <td style="text-align:center;padding:25px">Edad</td>
          <td style="text-align:center;padding:25px">Curso</td>
      </tr>
      <?php foreach ($resultado2 as $usuario) {
        echo "<tr>";
        echo "<td style='text-align:center'>".$usuario["id"]."</td>";
        echo "<td style='text-align:center'>".$usuario["nombre"]."</td>";
        echo "<td style='text-align:center'>".$usuario["apellidos"]."</td>";
        echo "<td style='text-align:center'>".$usuario["telefono"]."</td>";
        echo "<td style='text-align:center'>".$usuario["edad"]."</td>";
        echo "<td style='text-align:center'>".$usuario["curso"]."</td>";
      }
      ?>
    </tr>
  </table>
  <?php require "assets/footer.php" ?>
  </body>
</html>
