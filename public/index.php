<?php
require "../src/conexion.php";
require "../src/usuario.php";
require "../src/juego.php";
  $c=new Usuario();
  $resultado=$c->user();
  $c->sumar();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Proyecto</title>
    <link rel="stylesheet" href="css/master.css">
  </head>
  <body>
<?php require "assets/header.php" ?>
      <div class="div1in">
      <form>
      <a class="div2in">Fila <input id="fila" type="text"></a>
      <br><br>
      <a class="div3in">Columna <input id="columna" type="text"></a>
      <br><br>
      <input class="confirmar" type="button" value="Confirmar" onclick="return puntuacion()">
      <?php $puntos="<script>puntos</script>"; ?>
      </form>
      <br><br>
      <a>Movimientos realizados</a>
      <input id="mov" type="text" value="0" disabled>
      <br><br>
      <a>Número de movimientos</a>
      <input id="nmov" type="text" disabled>
      <br><br>
      <a>Mensaje</a>
      <input id="mensaje" type="text" disabled>
      </div>
      <div class="tablero">
        <script type="text/javascript" src="js/tablero.js"></script>
      </div>
    <?php require "assets/footer.php" ?>
  </body>
</html>
